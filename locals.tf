locals {
  subnets   = cidrsubnets(var.address_space, 4, 4, 4)

  subnet    = {
    public  = [local.subnets[0]]
    private = [local.subnets[1]]
    mysql   = [local.subnets[2]]
  }

  tags      = merge(var.tags, {
    source  = "terraform"
  })
}
