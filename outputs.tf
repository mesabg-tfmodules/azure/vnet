output "virtual_network_id" {
  value       = azurerm_virtual_network.this.id
  description = "Virtual network identifier"
}

output "virtual_network_guid" {
  value       = azurerm_virtual_network.this.guid
  description = "Virtual network guid"
}

output "public_subnet_id" {
  value       = azurerm_subnet.public.id
  description = "Virtual network public subnet identifier"
}

output "private_subnet_id" {
  value       = azurerm_subnet.private.id
  description = "Virtual network private subnet identifier"
}

output "mysql_subnet_id" {
  value       = azurerm_subnet.mysql.id
  description = "Virtual network mysql subnet identifier"
}
