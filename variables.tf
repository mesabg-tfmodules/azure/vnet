variable "name" {
  type      = string
  description = "General project name"
}

variable "resource_group_name" {
  type        = string
  description = "Resource group name"
}

variable "address_space" {
  type        = string
  description = "Cidr range for the Virtual Network"
  default     = "10.0.0.0/16"
}

variable "tags" {
  type        = map
  description = "Additional default tags"
  default     = {}
}
