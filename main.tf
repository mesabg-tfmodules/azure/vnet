resource "azurerm_virtual_network" "this" {
  name                = "vnet-${var.name}"
  address_space       = [var.address_space]
  location            = data.azurerm_resource_group.this.location
  resource_group_name = data.azurerm_resource_group.this.name

  tags                = local.tags
}

# Subnets: full network splitted in /20 from /16
resource "azurerm_subnet" "public" {
  name                              = "subnet-${var.name}-public"
  resource_group_name               = data.azurerm_resource_group.this.name
  virtual_network_name              = azurerm_virtual_network.this.name
  address_prefixes                  = local.subnet.public
  private_endpoint_network_policies = "Enabled"
}

resource "azurerm_subnet" "private" {
  name                              = "subnet-${var.name}-private"
  resource_group_name               = data.azurerm_resource_group.this.name
  virtual_network_name              = azurerm_virtual_network.this.name
  address_prefixes                  = local.subnet.private
  private_endpoint_network_policies = "Enabled"
}

resource "azurerm_subnet" "mysql" {
  name                              = "subnet-${var.name}-mysql"
  resource_group_name               = data.azurerm_resource_group.this.name
  virtual_network_name              = azurerm_virtual_network.this.name
  address_prefixes                  = local.subnet.mysql
  private_endpoint_network_policies = "Enabled"

  delegation {
    name                            = "Microsoft.DBforMySQL/flexibleServers"
    service_delegation {
      name                          = "Microsoft.DBforMySQL/flexibleServers"
      actions                       = ["Microsoft.Network/virtualNetworks/subnets/join/action"]
    }
  }
}

# Create public IP
resource "azurerm_public_ip" "this" {
  name                = "${var.name}-public-ip"
  location            = data.azurerm_resource_group.this.location
  resource_group_name = data.azurerm_resource_group.this.name
  allocation_method   = "Static"
  sku                 = "Standard"
  zones               = [1]

  tags                = local.tags
}

# NAT Gateway
resource "azurerm_nat_gateway" "this" {
  name                    = "${var.name}-nat"
  location                = data.azurerm_resource_group.this.location
  resource_group_name     = data.azurerm_resource_group.this.name
  sku_name                = "Standard"
  idle_timeout_in_minutes = 10
  zones                   = [1]

  tags                    = local.tags
}

# NAT - Public IP Association
resource "azurerm_nat_gateway_public_ip_association" "this" {
  nat_gateway_id       = azurerm_nat_gateway.this.id
  public_ip_address_id = azurerm_public_ip.this.id
}

# NAT - Subnets association
resource "azurerm_subnet_nat_gateway_association" "this" {
  subnet_id      = azurerm_subnet.private.id
  nat_gateway_id = azurerm_nat_gateway.this.id
}
